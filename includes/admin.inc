<?php

/**
 * Page callback for features summary.
 */
function features_summary_ui() {
  $features_components = features_get_components();
  $header = array(t('Component'), t('Exported in'));
  $output = array();
  foreach (features_get_component_map() as $element => $components) {
    $rows = array();
    foreach ($components as $component_name => $component) {
      $rows[$component_name]['data'][0] = $component_name;
      foreach ($component as $key => $feature_name) {
        $rows[$component_name]['data'][1][] = $feature_name;
      }
      $rows[$component_name]['data'][1] = implode(', ', $rows[$component_name]['data'][1]);
      $feature[] = $rows[$component_name]['data'][1];
    }
    // Sort rows by field name.
    ksort($rows);
    $output [] = array(
      '#theme' => 'table',
      '#prefix' => '<h2>' . $features_components[$element]['name'] . '</h2>',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No components have been exported yet.'),
    );
  }
  return $output;
}
